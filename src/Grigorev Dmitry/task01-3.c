#include <stdio.h>
#define _CRT_SECURE_NO_WARNINGS
int n;
char c;
int main() {
	n = 0;
	while ((c = getchar()) != '\n')
		if( c <= '9' && c >= '0')
			n += c - '0';
	printf("%i", n);
	return 0;
}
